package az.task;
import az.task.domain.AjaxDataTableRequest;
import az.task.domain.AjaxDataTableResult;
import az.task.domain.TaskDomain;
import az.task.domain.TaskRegister;
import az.task.service.TaskService;
import az.task.util.TaskConverter;
import az.task.validator.TaskRegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RequestMapping("/")
@Controller
public class WebController {
    private AjaxDataTableRequest ajaxDataTableRequest;

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskConverter taskConverter;
    @Autowired
    private TaskRegisterValidator taskRegisterValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        if (binder.getTarget() != null && binder.getTarget().getClass().equals(TaskDomain.class)) {
            binder.setValidator(taskRegisterValidator);
        }


    }

    @GetMapping("/getTaskName")
    @ResponseBody
    public boolean getTaskName(@RequestParam(name = "value") String taskName) {
        return taskService.getTaskExist(taskName);
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/getTask")
    @ResponseBody
    public AjaxDataTableResult personData(@RequestParam(name = "draw") long draw, @RequestParam(name = "start") long start,
                                          @RequestParam(name = "length") long length, @RequestParam(name = "search[value]") String searchValue,
                                          @RequestParam(name = "order[0][dir]") String orderDir, @RequestParam(name = "order[0][column]") long orderColumn) {
        ajaxDataTableRequest = new AjaxDataTableRequest();
        ajaxDataTableRequest.setDraw(draw);
        ajaxDataTableRequest.setStart(start);
        ajaxDataTableRequest.setLength(length);
        ajaxDataTableRequest.setColumnNumber(orderColumn);
        ajaxDataTableRequest.setOrderDirection(orderDir);
        ajaxDataTableRequest.setSearchValue(searchValue);
        return taskService.getAjaxDataTableResult(ajaxDataTableRequest);
    }


    @GetMapping("/taskForm")
    public ModelAndView taskForm() {
        ModelAndView modelAndView = new ModelAndView();
        TaskRegister taskRegister = new TaskRegister();
        modelAndView.addObject("taskObject", taskRegister);
        modelAndView.setViewName("taskForm");
        return modelAndView;
    }


    @PostMapping("/taskForm")
    public String taskForm2(@ModelAttribute("taskObject") @Validated TaskDomain taskDomain, BindingResult result) {

        if (result.hasErrors()) {

            return "taskForm";
        } else {
            taskService.insertTask(taskDomain);
            return "redirect:/";
        }
    }

    @GetMapping("/ajax")
    public String ajax() {
        return "testajax";
    }

    @GetMapping("/getAjax")
    @ResponseBody
    public TaskRegister getAjax() {
        ModelAndView modelAndView = new ModelAndView();
        TaskRegister taskRegister = new TaskRegister();
        taskRegister.setTaskOwner("Kamran Murtuzov");
        taskRegister.setTaskDescription("Bu sadece Task Descriptiondu");
        taskRegister.setTaskName("Taskin adi Yoxdur !");
        taskRegister.setStartDate("01.01.2000");
        taskRegister.setEndDate("02.02.2002");
        modelAndView.addObject("objectTask", taskRegister);
        return taskRegister;
    }


    @GetMapping("/getSingleTask")
    public ModelAndView getSingleTask(@RequestParam(name = "id") long id) {
        ModelAndView modelAndView = new ModelAndView();
        List<TaskDomain> list = taskService.getSingleTask(id);
        if (!list.isEmpty()) {
            TaskDomain taskDomain = list.get(0);
            modelAndView.addObject("task", taskDomain);
            modelAndView.setViewName("viewtask");
        } else {
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @GetMapping("/deleteTask")
    public String deleteTask(@RequestParam(name = "id") long id) {
        try {
            taskService.deleteTaskById(id);
            return "redirect:/";
        } catch (Exception e) {
            System.out.println("Exceptiona girdi !!");
            return "error";
        }
    }

    @GetMapping("/editTask")
    public ModelAndView editTask(@RequestParam(name = "id", required = false) long id) {
        ModelAndView modelAndView = new ModelAndView();

        List<TaskDomain> list = taskService.getSingleTask(id);

        if (!list.isEmpty()) {
            TaskDomain taskDomain = list.get(0);
            //taskConverter.getTaskRegister(list.get(0));

            modelAndView.addObject("editTaskObject", taskDomain);
            modelAndView.setViewName("edittask");

            System.out.println("Get Mapping edit task =" + taskDomain);
        } else {
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @PostMapping("/edittask")
    public String edittask2(@ModelAttribute(name = "editTaskObject") @Validated TaskDomain taskDomain, BindingResult result,
                            @RequestParam(name = "id") long id) {


        if (!result.hasErrors()) {
            System.out.println("Task Register unvana catdi !!!");
            //   TaskDomain taskDomain = taskConverter.getTaskDomain(taskRegister);
            try {
                taskService.editTaskById(taskDomain);
                return "redirect:/";
                //redirect
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return "redirect:/error";
            }
        } else {
            return "edittask";
        }


    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }

    @GetMapping("/viewtaskrest")
    public String viewtaskajax() {

        return "viewtaskrest";
    }
}
