package az.task.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class AjaxDataTableRequest implements Serializable {
    private static final long serialVersionUID = 2942579257964768057L;

    private long draw;
    private long start;
    private long length;
    private String searchValue;
    private long columnNumber;
    private String orderDirection;

    public static void sad(){}


    @Override
    public String toString() {
        return "AjaxDataTable{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                ", searchValue='" + searchValue + '\'' +
                ", columnNumber=" + columnNumber +
                ", orderDirection='" + orderDirection + '\'' +
                '}';
    }

    public AjaxDataTableRequest() {
    }

    public AjaxDataTableRequest(long draw, long start, long length, String searchValue, long columnNumber, String orderDirection) {
        this.draw = draw;
        this.start = start;
        this.length = length;
        this.searchValue = searchValue;
        this.columnNumber = columnNumber;
        this.orderDirection = orderDirection;
    }

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public long getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(long columnNumber) {
        this.columnNumber = columnNumber;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }
}
