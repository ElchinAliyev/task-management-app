package az.task.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class AjaxDataTableResult implements Serializable {
    private static final long serialVersionUID = 7756258478841313558L;
    private long draw;
    private long recordsFiltered;
    private long recordsTotal;
    private Object[][] data;

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Object[][] getData() {
        return data;
    }

    public void setData(Object[][] data) {
        this.data = data;
    }

    public AjaxDataTableResult(long draw, long recordsFiltered, long recordsTotal, Object[][] data) {
        this.draw = draw;
        this.recordsFiltered = recordsFiltered;
        this.recordsTotal = recordsTotal;
        this.data = data;
    }

    public AjaxDataTableResult() {
    }
}
