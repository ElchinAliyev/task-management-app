package az.task.repository;



import az.task.domain.TaskDomain;

import java.util.List;

public interface TaskRepository {

    public long getTotalCount();
    public long getFilteredCount(String search);
    public List<TaskDomain> getPersonData(String search, Long column, String columnDir, Long start, Long end);
    public void insertTask(TaskDomain taskDomain);
    public List<TaskDomain> getSingleTask(long id);
    public void deleteTaskById(long id);
    public void editTaskById(TaskDomain taskDomain);
    public boolean getTaskName(String taskName);
}
