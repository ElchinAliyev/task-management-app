package az.task.repository.impl;
import az.task.domain.TaskDomain;
import az.task.repository.TaskRepository;
import az.task.repository.jdbc.SqlQuery;
import az.task.util.ColumnNames;
import az.task.util.TaskRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private TaskRowMapper taskRowMapper;
    @Override
    public long getTotalCount() {
        return  jdbcTemplate.queryForObject(SqlQuery.GET_TOTAL_COUNT,new MapSqlParameterSource(),Long.class);
    }

    /*
    *
    *
    * "select count(id) from Person_Works " +
        "where concat(Task_Name,Task_Description,Full_Name,Start_Date,End_Date) LIKE :searchWord "
    *
    * */

    @Override
    public long getFilteredCount(String search) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("searchWord","%"+search+"%");
        return  jdbcTemplate.queryForObject(SqlQuery.GET_FILTERED_COUNT,sqlParameterSource,Long.class);
    }


    /*
    *
    *
    * "select Task_Name , Task_Description , Full_Name , Start_Date ," +
        " End_Date from Person_Works " +
        "where concat(Task_Name,Task_Description,Full_Name,Start_Date,End_Date) LIKE :searchWord " +
        " order by :columnName :columnDir " +
        " Limit :start , :end ";
    *
    *
    *
    * */
    @Override
    public List<TaskDomain> getPersonData(String search, Long column, String columnDir, Long start, Long end) {
        System.out.println(column);
        System.out.println(columnDir);
        System.out.println(search);

        System.out.println("---------------");
        System.out.println(ColumnNames.whichColumn(column));
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("searchWord", "%"+search+"%");
        String sql = String.format(SqlQuery.GET_PERSON_DATA,ColumnNames.whichColumn(column),columnDir,start,end);

        System.out.println(sql);
        return jdbcTemplate.query(sql, sqlParameterSource, taskRowMapper);

    }
/*
* "Insert into Person_Works(id,Task_Name,Task_Description," +
        "Full_Name,Start_Date,End_Date) VALUES (null, :taskName, :taskDescription, :fullName, :startDate, :endDate)";
*
* */
    @Override
    public void insertTask(TaskDomain taskDomain) {
        SqlParameterSource sqlParameterSource =  new MapSqlParameterSource()
                .addValue("taskName",taskDomain.getTaskName())
                .addValue("taskDescription",taskDomain.getTaskDescription())
                .addValue("fullName",taskDomain.getTaskOwner())
                .addValue("startDate",taskDomain.getStartDate())
                .addValue("endDate",taskDomain.getEndDate());
        KeyHolder keyHolder = new GeneratedKeyHolder();
      int count =  jdbcTemplate.update(SqlQuery.INSERT_TASK,sqlParameterSource,keyHolder);
      if (count > 0){
          long id = keyHolder.getKey().longValue();
          System.out.println(id +" DB - ya elave edilmisdir !!");
      }
    }

    /*
    *
    *  select id , Task_Name,Task_Description,Full_Name,Start_Date,End_Date from Person_Works
         where id = :id ;
    *
    * */


    @Override
    public List<TaskDomain> getSingleTask(long id) {
        HashMap hashMap = new HashMap();
         hashMap.put("id",id);
      List<TaskDomain>  list =  jdbcTemplate.query(SqlQuery.GET_TASK_BY_ID,hashMap,taskRowMapper);
      return list;
    }
/*
*
*  Delete from Person_Works where id = :id ;
*
* */
    @Override
    public void deleteTaskById(long id) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id",id);
     int count = jdbcTemplate.update(SqlQuery.DELETE_TASK_BY_ID,sqlParameterSource);
        if (count>0){
            System.out.println(count + " setir silindi!");
        }
        else{
            throw  new RuntimeException("Setir silinemedi");
        }
    }
/**
 * " Update Person_Works Set Task_Name = :taskName ," +
 *         " Task_Description = :taskDescription , " +
 *         " Full_Name = :taskOwner , Start_Date= :startDate , End_Date = :endDate where id = :id ";
 *
 * */
    @Override
    public void editTaskById(TaskDomain taskDomain) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("taskName",taskDomain.getTaskName())
                .addValue("taskDescription",taskDomain.getTaskDescription())
                .addValue("taskOwner",taskDomain.getTaskOwner())
                .addValue("startDate",taskDomain.getStartDate())
                .addValue("endDate",taskDomain.getEndDate())
                .addValue("id",taskDomain.getId());
        System.out.println(taskDomain);

       int count = jdbcTemplate.update(SqlQuery.UPDATE_TASK_BY_ID,sqlParameterSource);
       if (count > 0){
           System.out.println("Update ugurla basa catdi !");
       }
       else
       {
           throw new RuntimeException("Update Zamani error bas verdi !");
       }

    }
/*
* "select Task_Name from Person_Works where Task_Name = :taskName "
*
* */
    @Override
    public boolean getTaskName(String taskName) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("taskName",taskName);
     List<TaskDomain> list = jdbcTemplate.query(SqlQuery.getTaskName,sqlParameterSource,taskRowMapper);
     if (list.isEmpty()){
         return false;
     }
     else {
         return true;
     }
    }
}
