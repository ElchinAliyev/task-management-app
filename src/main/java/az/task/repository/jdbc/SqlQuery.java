package az.task.repository.jdbc;

public class SqlQuery {
public static final String GET_TOTAL_COUNT ="select count(id) from Person_Works";
public static final String GET_PERSON_DATA = "select id ,Task_Name , Task_Description , Full_Name , Start_Date ," +
        " End_Date from Person_Works " +
        "where concat(Task_Name,Task_Description,Full_Name,Start_Date,End_Date) LIKE :searchWord " +
        " order by %s %s " +
        " Limit %d , %d ";

public static final String GET_FILTERED_COUNT = "select count(id) from Person_Works " +
        "where concat(Task_Name,Task_Description,Full_Name,Start_Date,End_Date) LIKE :searchWord ";

public static final String INSERT_TASK = "Insert into Person_Works(id,Task_Name,Task_Description," +
        "Full_Name,Start_Date,End_Date) VALUES (null, :taskName, :taskDescription, :fullName, :startDate, :endDate)";

public static final String GET_TASK_BY_ID = " select id , Task_Name,Task_Description,Full_Name,Start_Date,End_Date " +
        " from Person_Works where id = :id ";

public static final String DELETE_TASK_BY_ID= " Delete from Person_Works where id = :id ";
public static final String UPDATE_TASK_BY_ID = " Update Person_Works Set Task_Name = :taskName ," +
        " Task_Description = :taskDescription , " +
        " Full_Name = :taskOwner , Start_Date= :startDate , End_Date = :endDate  where id = :id ";
public static final String getTaskName = "select id ,Task_Name , Task_Description , Full_Name , Start_Date , End_Date " +
        " from Person_Works where Task_Name = :taskName ";

}
