package az.task.rest;


import az.task.domain.*;
import az.task.repository.*;
import az.task.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RequestMapping("/rest/task/")
@RestController
public class TaskRestController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskRepository taskRepository;

    @ResponseStatus(HttpStatus.ACCEPTED)
    @GetMapping(value = {"/{taskId}", "/"})
    public List<TaskDomain> getTaskList(@PathVariable(name = "taskId") Optional<Long> taskId) {
        List<TaskDomain> taskDomains = null;
        System.out.println("taskId: " + taskId);
        if (taskId.isPresent()) {
            taskDomains = taskService.getSingleTask(taskId.get());
        } else {
            System.out.println("Id Bosdur");
        }

        if (taskDomains != null) {
            return taskDomains;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Bele id-li task tapilmadi");
        }
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public void insertTask(@RequestBody TaskDomain taskDomain) {
        System.out.println("Rest insert geldi " + taskDomain);
        taskService.insertTask(taskDomain);

    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping("/{taskId}")
    public void updateTask(@PathVariable(name = "taskId") long taskId,
                           @RequestBody TaskDomain task) {
        List<TaskDomain> taskDomainList = taskService.getSingleTask(taskId);
        if (!taskDomainList.isEmpty()) {
            taskService.editTaskById(task);
            System.out.println("Task update edildi !");
        } else {
            taskService.insertTask(task);
            System.out.println("Task Insert edildi !");
            throw new ResponseStatusException(HttpStatus.CREATED, "task yaradildi !");

        }


    }


    @DeleteMapping("/{taskId}")
    public void deleteTask(@PathVariable(name = "taskId") long taskId) {
        taskService.deleteTaskById(taskId);
        System.out.println("Task Silindi");

    }

    @GetMapping("/getAllTasks")
    public List<TaskDomain> getAllTasks(@RequestParam(name = "search", defaultValue = "", required = false) String search,
                                        @RequestParam(name = "searchCol", defaultValue = "1") String searchCol,
                                        @RequestParam(name = "searchDir", required = false, defaultValue = "asc") String dir,
                                        @RequestParam(name = "start", defaultValue = "0", required = false) long start,
                                        @RequestParam(name = "end", defaultValue = "10", required = false) long end) {
        return taskRepository.getPersonData(search, Long.valueOf(searchCol), dir, start, end);

    }

}
