package az.task.service;


import az.task.domain.AjaxDataTableRequest;
import az.task.domain.AjaxDataTableResult;
import az.task.domain.TaskDomain;

import java.util.List;

public interface TaskService {


    public AjaxDataTableResult getAjaxDataTableResult(AjaxDataTableRequest request);
    public void insertTask(TaskDomain task);
    public List<TaskDomain> getSingleTask(long id);
    public void deleteTaskById(long id);
    public void editTaskById(TaskDomain taskDomain);
    public boolean getTaskExist(String taskName);

}
