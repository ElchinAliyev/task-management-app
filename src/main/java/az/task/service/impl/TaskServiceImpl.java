package az.task.service.impl;

import az.task.domain.*;
import az.task.domain.AjaxDataTableRequest;
import az.task.domain.AjaxDataTableResult;
import az.task.repository.TaskRepository;
import az.task.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public AjaxDataTableResult getAjaxDataTableResult(AjaxDataTableRequest request) {
        AjaxDataTableResult result = new AjaxDataTableResult();
        result.setDraw(request.getDraw());
        long totalCount =  taskRepository.getTotalCount();
      result.setRecordsTotal(totalCount);
      long filteredCount = taskRepository.getFilteredCount(request.getSearchValue());
      result.setRecordsFiltered(filteredCount);
    List<TaskDomain> list =  taskRepository.getPersonData(request.getSearchValue(),request.getColumnNumber(),
              request.getOrderDirection(),request.getStart(), request.getLength());

    Object[][] object = new Object[list.size()][6];

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
if (!list.isEmpty()) {
    for (int i = 0; i < list.size(); i++) {
        object[i][0] = list.get(i).getTaskName();
        object[i][1] = list.get(i).getTaskDescription();
        object[i][2] = list.get(i).getTaskOwner();
        object[i][3] = dateTimeFormatter.format(list.get(i).getStartDate());
        object[i][4] = dateTimeFormatter.format(list.get(i).getEndDate());
       String link = "<a href=\"getSingleTask?id="+list.get(i).getId()+"\">View</a>  "+
               "<a class=\"delete\" href=\"deleteTask?id="+list.get(i).getId()+"\">Delete</a>  "+
               "<a href=\"editTask?id="+list.get(i).getId()+"\">Edit</a>";
        object[i][5] = link;
    }
}
        result.setData(object);

      return result;
    }
    @Transactional
    @Override
    public void insertTask(TaskDomain task) {
       taskRepository.insertTask(task);
    }

    @Override
    public List<TaskDomain> getSingleTask(long id) {
      List<TaskDomain> list =   taskRepository.getSingleTask(id);
      return list;
    }

    @Override
    public void deleteTaskById(long id) {
        taskRepository.deleteTaskById(id);
    }

    @Override
    public void editTaskById(TaskDomain taskDomain) {
        taskRepository.editTaskById(taskDomain);
    }

    @Override
    public boolean getTaskExist(String taskName) {
       return taskRepository.getTaskName(taskName);
    }
}
