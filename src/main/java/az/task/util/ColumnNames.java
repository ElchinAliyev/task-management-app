package az.task.util;

public enum ColumnNames {
    Task_Name(0),
    Task_Description(1),
    Full_Name(2),
    Start_Date(3),
    End_Date(4);

    private int value;

    ColumnNames(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ColumnNames whichColumn(long value) {
        ColumnNames columnNames = null;
        if (Task_Name.value == value)
            columnNames = Task_Name;
        else if (Task_Description.value == value)
            columnNames = Task_Description;
        else if (Full_Name.value == value)
            columnNames = Full_Name;
        else if (Start_Date.value == value)
            columnNames = Start_Date;
        else if (End_Date.value == value)
            columnNames = End_Date;
   return columnNames;
    }
}
