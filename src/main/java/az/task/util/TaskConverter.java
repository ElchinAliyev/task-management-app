package az.task.util;


import az.task.domain.TaskDomain;
import az.task.domain.TaskRegister;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class TaskConverter {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public TaskRegister getTaskRegister(TaskDomain taskDomain){
        TaskRegister taskRegister = new TaskRegister();
        taskRegister.setTaskName(taskDomain.getTaskName());
        taskRegister.setTaskOwner(taskDomain.getTaskOwner());
        taskRegister.setId(taskDomain.getId());
        taskRegister.setTaskDescription(taskDomain.getTaskDescription());
        taskRegister.setStartDate(dateTimeFormatter.format(taskDomain.getStartDate()));
        taskRegister.setEndDate(dateTimeFormatter.format(taskDomain.getEndDate()));
        return taskRegister;
    }
    public TaskDomain getTaskDomain(TaskRegister taskRegister){
        TaskDomain taskDomain = new TaskDomain();
        taskDomain.setId(taskRegister.getId());
        taskDomain.setTaskName(taskRegister.getTaskName());
        taskDomain.setTaskDescription(taskRegister.getTaskDescription());
        taskDomain.setTaskOwner(taskRegister.getTaskOwner());
        taskDomain.setStartDate(LocalDate.parse(taskRegister.getStartDate(),dateTimeFormatter));
        taskDomain.setEndDate(LocalDate.parse(taskRegister.getEndDate(),dateTimeFormatter));
        return taskDomain;
    }
}
