package az.task.util;
import az.task.domain.TaskDomain;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TaskRowMapper implements RowMapper<TaskDomain> {
    @Override
    public TaskDomain mapRow(ResultSet resultSet, int i) throws SQLException {
        TaskDomain task = new TaskDomain();
        task.setId(resultSet.getLong("id"));
        task.setTaskName(resultSet.getString("Task_Name"));
        task.setTaskDescription(resultSet.getString("Task_Description"));
        task.setTaskOwner(resultSet.getString("Full_Name"));
        task.setStartDate(resultSet.getDate("Start_Date").toLocalDate());
        task.setEndDate(resultSet.getDate("End_Date").toLocalDate());
        return task;
    }
}
