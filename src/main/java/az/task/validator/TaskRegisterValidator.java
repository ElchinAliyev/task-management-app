package az.task.validator;

import az.task.domain.TaskDomain;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class TaskRegisterValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {

        System.out.println("Yoxlama isledi ! ");
        return aClass.equals(TaskDomain.class);
    }

    @Override
    public void validate(Object o, Errors errors) {

        TaskDomain taskDomain = (TaskDomain) o;
        System.out.println("Validator isledi !! ");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "taskName", "taskName.error");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "taskDescription", "taskDescription.error");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "taskOwner", "taskOwner.error");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "startDate.error");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endDate", "endDate.error");

        if (!errors.hasErrors()) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            if (taskDomain.getTaskName().length() < 2 || taskDomain.getTaskName().length() > 20) {
                errors.rejectValue("taskName", "taskName.error2");

            }
            if (taskDomain.getTaskOwner().length() < 2 || taskDomain.getTaskOwner().length() > 20) {
                errors.rejectValue("taskOwner", "taskOwner.error2");
            }

            if (taskDomain.getStartDate() != null && taskDomain.getEndDate() != null) {
                LocalDate startDate = taskDomain.getStartDate();
                LocalDate endDate = taskDomain.getEndDate();

                if (!endDate.isAfter(startDate)) {
                    System.out.println("Error Start Date isledi!");
                    errors.rejectValue("startDate", "startDate.error2");
                }

            }

        }


    }
}
