<%@ page import="org.springframework.web.bind.annotation.ModelAttribute" %><%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 10.11.2019
  Time: 15:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"/>
<html>
<head>
    <style>

        .error {
            color: red;
        }

        h2 {

            margin-left: 550px;

        }
    </style>
    <title>Edit Task</title>
</head>
<body>
<h2>Edit Task</h2>
<div class="container">
<form:form modelAttribute="editTaskObject" action="edittask?id=${editTaskObject.id}" method="post" >
    <div class="row">

        <div class="col-lg-3">
        </div>
        <div class="col-lg-3">
            <br/>
            Task Name : <form:input  path="taskName" id="taskName"/>
            <form:errors cssClass="error" path="taskName"/>
        </div>
        <div class="col-lg-5">
            <br/>
            Task Description :<br/> <form:textarea path="taskDescription" id="taskDesc"/>
            <form:errors cssClass="error" path="taskDescription"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">

        </div>
        <div class="col-lg-3">

            Start Date : <form:input path="startDate" id="startDate" type="date"/>
            <form:errors cssClass="error" path="startDate"/>
        </div>
        <div class="col-lg-5">

            End Date :
            <br/><form:input path="endDate" id="endDate" type="date"/><br/>
            <form:errors cssClass="error" path="endDate"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-2">
            Task Owner :<br/> <form:input path="taskOwner" id="taskOwner"/>
            <form:errors cssClass="error" path="taskOwner"/><br/>
        </div>
        <div class="col-lg-1">
            <br/>
            <input type="submit" value="Submit"/>
        </div>
    </div>
</form:form>
</div>
</body>
</html>
