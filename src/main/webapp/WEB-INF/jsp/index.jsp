<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 09.11.2019
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Task Management Application</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <style>
        #addTask {
            margin: 25px 25px 25px 10px;
        }


    </style>
</head>
<body>
 <div id="addTask" ><a class="btn btn-success"  href="taskForm"> Add New Task </a> </div>
<table id="taskTable">
    <thead>
    <tr>
        <th>Task Name</th>
        <th>Task Description</th>
        <th>Task Owner</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>View</th>
    </tr>
    </thead>
</table>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#taskTable').DataTable({
            "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "processing": true,
            "serverSide": true,
            ajax : {
                async: false,
                url: "getTask"
            }
        });
        $(".delete").click(function() {
            if(!confirm("Are you sure for deleting ?")){
                return false;
            }
        });

    });


</script>
</body>
</html>
