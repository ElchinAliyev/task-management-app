<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 10.11.2019
  Time: 15:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"/>
<html>
<head>
    <style>
        .error {
            color: red;
        }


    </style>
    <title>Add New Task</title>
</head>
<body>
<br/>
<h2 style="margin-left: 500px;">Create New Task</h2>
<div class="container">
<form:form modelAttribute="taskObject" action="taskForm" method="post">
    <div class="row">

        <div class="col-lg-3">
        </div>
        <div class="col-lg-3">
            <br/>
            Task Name : <form:input  path="taskName" id="taskName"/>
            <form:errors cssClass="error" path="taskName"/>
        </div>
        <div class="col-lg-5">
            <br/>
            Task Description :<br/> <form:textarea path="taskDescription" id="taskDesc"/>
            <form:errors cssClass="error" path="taskDescription"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">

        </div>
        <div class="col-lg-3">

            Start Date : <form:input path="startDate" id="startDate" type="date"/>
            <form:errors cssClass="error" path="startDate"/>
        </div>
        <div class="col-lg-5">

            End Date :
            <br/><form:input path="endDate" id="endDate" type="date"/><br/>
            <form:errors cssClass="error" path="endDate"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-2">
        Task Owner :<br/> <form:input path="taskOwner" id="taskOwner"/>
        <form:errors cssClass="error" path="taskOwner"/><br/>
        </div>
        <div class="col-lg-1">
            <br/>
            <input type="submit" value="Submit"/>
        </div>
    </div>


</form:form>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#taskName").blur(function () {
                console.log("Ajax Request isledi !")
                $.ajax({
                    url: "getTaskName?value="+$("#taskName").val(),
                    success:function (data) {
                        if  (data){
                            console.log("tekrardi!!!!");
                        }
                        else {
                            console.log("tekrar deyil !");
                        }
                    }
                })
            })
            });

        console.log("gele buda ayri sey !!! ");

       //     $("#")
            
    </script>
</body>
</html>
