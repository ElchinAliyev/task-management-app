<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 10.11.2019
  Time: 15:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>View Task</title>
</head>
<body>
Task ID : ${task.id} <br/>
Task Name : ${task.taskName} <br/>
Task Description : ${task.taskDescription}<br/>
Task Owner : ${task.taskOwner}<br/>
Start Date : ${task.startDate}<br/>
End Date : ${task.endDate}<br/>
<h3></h3>
</body>
</html>
